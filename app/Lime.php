<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lime extends Model
{
    protected $fillable = [
        'lime','nonlime'
    ];
}
