<?php

namespace App\Http\Controllers;

use App\Item;
use App\Lime;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Charts\SimpleChart;

class DashboardController extends Controller
{
    public function __construct(){
        $this->middleware(['auth']);
    }

    public function index(Request $request){
        // $clime = Lime::orderby('created_at', 'desc')->paginate(25);
        // $lime = Lime::groupby('lime')->get(['lime']);
        // $labels = $lime->pluck('created_by');
        // $nlime = Lime::groupby('nonlime')->get(['nonlime']);
        // return view('dashboard',['climes' => $clime, 'lime'=> $lime, 'nonlime'=> $nlime]);
        // $lime = Lime::latest()->paginate(15);

        // $chart = new SimpleChart;
        // $chart->lable(['one','two','three']);
        // $chart->dataset('My lime', 'line', [1,2,3]);

        $clime = Lime::orderby('created_at', 'desc')->paginate(25);

        $lime = Lime::latest()->take(15)->get();
        $labels = $lime->pluck('id');
        $data = $lime->pluck('lime');

        return view('dashboard', compact('labels','data'));

    }

    public function history() {
        $clime = Lime::orderby('created_at', 'desc')->paginate(25);
        return view('history',['climes' => $clime]);
    }
}
