<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function list(){

        return Item::get();
    }

    public function add(Request $request){

        $item = new Item;
        $item->found = $request->found;
        $item->qty = $request->qty;

        $result = $item->save();
        if($result)
        {
            return ["Result"=>"Data has been sent"];
        }
        else{
            return ["Result"=>"Error"];
        }
    }

    public function update(Request $request){

        $item = Item::find($request->id);
        $item->found = $request->found;
        $item->qty = $request->qty;

        $result = $item->save();
        if($result)
        {
            return ["Result"=>"Data has been sent"];
        }
        else{
            return ["Result"=>"Error"];
        }
    }

    public function delete($id){
        $item = Item::find($id);
        $result = $item->delete();
        if($result)
        {
            return ["Result"=>"Record has been deleted"];
        }
        else{
            return ["Result"=>"Error"];
        }
    }
}
