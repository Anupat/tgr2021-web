<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function display(){
        return User::get();
    }

    public function index(){
        return response()->json(User::get());
    }
}
