<?php

namespace App\Http\Controllers;

use App\Lime;
use Illuminate\Http\Request;
use Carbon\Carbon;

class LimeController extends Controller
{
    public function all(){
        return Lime::all();
    }

    public function lime(){
        return Lime::where('type', 'lime')->get();
    }
    public function non_lime(){
        return Lime::where('type', 'non-lime')->get();
    }

    public function add(Request $request){
        $lime = new Lime;
        $lime->lime = $request->lime;
        $lime->nonlime = $request->nonlime;
        $result = $lime->save();

        if($result){
            return ['result' => 'Data has been sent.'];
        }
        else {
            return ['result' => 'Something error.'];
        }
    }

    public function chart(){
        $lime = Lime::latest()->take(15)->orderby('id','asc')->get();
        $labels = $lime->pluck('created_at');
        $data = $lime->pluck('lime');
        return response()->json(compact('labels','data'));
        // $clime = Lime::orderby('created_at', 'desc')->take(15)->get();
        // return response()->json(compact('clime'));


    }

    public function chart2(){
        $lime = Lime::latest()->take(15)->orderby('id','asc')->get();
        $labels = $lime->pluck('created_at');
        $data = $lime->pluck('nonlime');
        return response()->json(compact('labels','data'));
        // $clime = Lime::orderby('created_at', 'desc')->take(15)->get();
        // return response()->json(compact('clime'));


    }
}
