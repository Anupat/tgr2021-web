<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LimeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('list', [UserController::class, 'index']);

Route::get('lime', [LimeController::class, 'lime'])->name('lime');
Route::get('non-lime', [LimeController::class, 'non-lime'])->name('non-lime');

Route::get('dashboard',[DashboardController::class, 'index'])->name('dashboard');
Route::get('history',[DashboardController::class, 'history'])->name('history');

Route::get('/register', [RegisterController::class, 'index'])->name('register');
Route::post('/register', [RegisterController::class, 'store']);

Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class, 'request']);

Route::post('/logout', [LogoutController::class, 'request'])->name('logout');
