<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\LimeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LimeSendController;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('team-list', [UserController::class, 'display']);

// Route::get('db',[DashboardController::class, 'index'])
// Route::get('chart', [DashboardController::class, 'chart']);

Route::post('add', [LimeController::class, 'add']);
Route::get('all', [LimeController::class, 'all']);
Route::get('lime', [LimeController::class, 'lime']);
Route::get('non-lime', [LimeController::class, 'non-lime']);

Route::get('chart', [LimeController::class, 'chart']);
Route::get('chart2', [LimeController::class, 'chart2']);


Route::get('list', [ItemController::class, 'list']);
Route::post('insert', [ItemController::class, 'add']);
Route::put('update', [ItemController::class, 'update']);
Route::delete('delete/{id}',[ItemController::class, 'delete']);