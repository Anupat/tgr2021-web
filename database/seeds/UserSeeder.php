<?php

use App\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        // $users = [
        //     ['id' => 1, 'firstname' => 'Anupat', 'lastname' => 'Chantarasukkasem', 'email' => Str::random(10).'@gmail.com', 'password' => Hash::make('password'),],
        //     ['id' => 2, 'firstname' => 'Mattawan', 'lastname' => 'Soonthornsatitwong', 'email' => Str::random(10).'@gmail.com', 'password' => Hash::make('password'),],
        //     ['id' => 3, 'firstname' => 'Nuntakorn', 'lastname' => 'Sopap', 'email' => Str::random(10).'@gmail.com', 'password' => Hash::make('password'),],
        //     ['id' => 4, 'firstname' => 'Taksaorn', 'lastname' => 'Aksornsin', 'email' => Str::random(10).'@gmail.com', 'password' => Hash::make('password'),],
        //     ['id' => 5, 'firstname' => 'Chanon', 'lastname' => 'Thamrongchaiskul', 'email' => Str::random(10).'@gmail.com', 'password' => Hash::make('password'),],
        // ];

        // User::create($users);

        DB::table('users')->insert([
            'firstname' => 'Anupat',
            'lastname' => 'Chantarasukkasem',
            'username' => 'Anupat',
            'email' => 'imovie.imac@hotmail.com',
            'password' => Hash::make('qwer'),
        ]);

        DB::table('users')->insert([
            'firstname' => 'Mattawan',
            'lastname' => 'Soonthornsatitwong',
            'username' => 'Mattawan',
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'firstname' => 'Nuntakorn',
            'lastname' => 'Sopap',
            'username' => 'Nuntakorn',
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'firstname' => 'Taksaorn',
            'lastname' => 'Aksornsin',
            'username' => 'Taksaorn',
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'firstname' => 'Chanon',
            'lastname' => 'Thamrongchaiskul',
            'username' => 'Chanon',
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
        ]);

    }
}
