<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	DB::table('items')->insert([
		'found' => "Apple",
		'qty' => 1
	]);
        DB::table('items')->insert([
                'found' => "Papaya",
                'qty' => 1
        ]);
        DB::table('items')->insert([
                'found' => "Orange",
                'qty' => 2
        ]);
        DB::table('items')->insert([
                'found' => "Banana",
                'qty' => 2
        ]);

    }
}
