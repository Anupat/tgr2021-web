<?php

use Illuminate\Database\Seeder;
use App\Items;

class ItemSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	$item = new Items();
	$item->found = 'lime';
	$item->qty = 99;
	$item->save();
    }
}
