@extends('topnavlayout.layout')

@section('content')
<main class="bg-gray-100">
    <div class="mt-6 flex justify-center">
        <div class="bg-white w-4/12 shadow-md p-6 rounded-xl">
            <p href="" class="font-semibold text-3xl mb-3">Login</p>
            @if (session('status'))
                <div class="p-2 bg-red-100 text-red-500 w-full rounded-md mb-3">
                    {{ session('status') }}
                </div>
            @endif
            <form action="{{ route('login') }}" method="POST">
                @csrf
                <div class="mb-4">
                    <label for="email" class="">E-mail</label>
                    <input type="text" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('email') border-red-500 @enderror" id="email" name="email" placeholder="Enter email" value="{{ old('email') }}">
                    @error('email')
                        <div class="text-red-500 mt-2 text-sm">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="password" class="">Password</label>
                    <input type="password" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('password') border-red-500 @enderror" id="password" name="password" placeholder="Choose a password" value="">
                    @error('password')
                        <div class="text-red-500 mt-2 text-sm">{{ $message }}</div>
                    @enderror
                </div>
                <div>
                    <button type="submit" class="bg-blue-500 text-white px-4 py-3 font-medium w-full rounded-xl">Login</button>
                </div>
            </form>
        </div>
    </div>
</main>
@endsection
