@extends('topnavlayout.layout')

@section('content')
<main class="bg-gray-100">
    <div class="pt-6 flex justify-center">
        <div class="bg-white w-4/12 shadow-md p-6 rounded-xl">
            <p href="" class="font-semibold text-3xl mb-3">Register</p>
            <form action="{{ route('register') }}" method="POST">
                @csrf
                <div class="mb-4">
                    <label for="firstname" class="">Firstname</label>
                    <input type="text" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('name') border-red-500 @enderror" id="firstname" name="firstname" placeholder="Your firstname" value="{{ old('firstname') }}">
                    @error('firstname')
                        <div class="text-red-500 mt-2 text-sm">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="lastname" class="">Lastname</label>
                    <input type="text" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('name') border-red-500 @enderror" id="lastname" name="lastname" placeholder="Your lastname" value="{{ old('lastname') }}">
                    @error('lastname')
                        <div class="text-red-500 mt-2 text-sm">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="username" class="">Username</label>
                    <input type="text" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('username') border-red-500 @enderror" id="username" name="username" placeholder="Username" value="{{ old('username') }}">
                    @error('username')
                        <div class="text-red-500 mt-2 text-sm">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="email" class="">E-mail</label>
                    <input type="text" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('email') border-red-500 @enderror" id="email" name="email" placeholder="Enter email" value="{{ old('email') }}">
                    @error('email')
                        <div class="text-red-500 mt-2 text-sm">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="password" class="">Password</label>
                    <input type="password" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('password') border-red-500 @enderror" id="password" name="password" placeholder="Choose a password" value="">
                    @error('password')
                        <div class="text-red-500 mt-2 text-sm">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="password_confirmation" class="">Comfirm Password</label>
                    <input type="password" class="bg-gray-100 border-2 w-full p-4 rounded-lg" id="password_confirmation" name="password_confirmation" placeholder="Repeat your password" value="">
                </div>

                <div>
                    <button type="submit" class="bg-blue-500 text-white px-4 py-3 font-medium w-full rounded-xl">Register</button>
                </div>
            </form>
        </div>
    </div>
</main>
@endsection
