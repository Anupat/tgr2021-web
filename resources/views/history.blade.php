<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <style>
            body {
                font-family: 'Nunito';
            }
        </style>

        <title>History</title>
    </head>
    <body class="text-gray-600 bg-gray-100 flex flex-row">
        <nav class="w-72 h-screen bg-gray-800">
            <div class="text-gray-200 p-3">
                <h1 class="text-4xl font-bold mb-4">
                    <a href="/" class="">TGR 19</a>
                </h1>
                <ul class="text-xl font-semibold">
                    <li class="mb-1 rounded -full hover:bg-white hover:text-gray-800">
                        <a href="{{ route('dashboard') }}">
                            <span class="pl-3 flex items-center py-0.5 ">
                                <svg class="w-6 h-6 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 5a1 1 0 011-1h14a1 1 0 011 1v2a1 1 0 01-1 1H5a1 1 0 01-1-1V5zM4 13a1 1 0 011-1h6a1 1 0 011 1v6a1 1 0 01-1 1H5a1 1 0 01-1-1v-6zM16 13a1 1 0 011-1h2a1 1 0 011 1v6a1 1 0 01-1 1h-2a1 1 0 01-1-1v-6z"></path></svg>
                                Dashboard
                            </span>
                        </a>
                    </li>
                    <li class="mb-1 rounded -full hover:bg-white hover:text-gray-800">
                        <a href="{{ route('history') }}">
                            <span class="pl-3 flex items-center py-0.5 ">
                                <svg class="w-6 h-6 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253"></path></svg>
                                History
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>

        <main class="mx-8 mt-4 w-screen">
            <!-- login button -->
            <div class="flex justify-between py-3 items-center">
                <div class="text-gray-400 text-4xl">History</div>
                <div class="flex">
                    @auth
                        <div class="mr-6 my-3 text-gray-700 text-xl justify-items-center">
                            {{ auth()->user()->firstname }} {{ auth()->user()->lastname }}
                        </div>
                        <div class="w-24 mr-6 text-yellow-500 rounded-full border-lg border-yellow-500 bg-white text-l font-semibold shadow">
                            <a href="" class="flex justify-center p-3">Setting</a>
                        </div>
                        <div class="w-24 text-yellow-500 rounded-full border-lg border-yellow-500 bg-white text-l font-semibold shadow">
                            <form action="{{ route('logout') }}" method="POST" class="flex justify-center p-3">
                                @csrf
                                <button type="submit" class="font-semibold">Logout</button>
                            </form>
                        </div>
                    @endauth
                    @guest
                        <div class="w-24 mr-6 text-yellow-500 rounded-full border-lg border-yellow-500 bg-white text-l font-semibold shadow">
                            <a href="{{ route('register') }}" class="flex justify-center p-3">Register</a>
                        </div>
                        <div class="w-24 text-yellow-500 rounded-full border-lg border-yellow-500 bg-white text-l font-semibold shadow">
                            <a href="{{ route('login') }}" class="flex justify-center p-3">Login</a>
                        </div>
                    @endguest
                    </div>
            </div>

            <!-- Pallet for select in catagory -->
            {{-- <div>
                <div class="mb-6">
                    <div class="py-5">
                        <main class="h-full overflow-y-auto">
                            <div class="px-10 mx-auto grid">
                                <!-- Cards -->
                                <div class="grid gap-6 mb-8 md:grid-cols-2 xl:grid-cols-4 3xl:grid-cols-5 4xl:grid-cols-6">
                                    <!-- Card 1 -->
                                    <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                                        <div class="p-3 mr-4 text-green-500 bg-green-100 rounded-full dark:text-orange-100 dark:bg-orange-500">
                                            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7"></path></svg>
                                        </div>
                                        <div>
                                        <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                                            Total limes
                                        </p>
                                        <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                                            {{ $lime }}
                                        </p>
                                        </div>
                                    </div>
                                    <!-- Card 2 -->
                                    <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                                        <div class="p-3 mr-4 text-red-500 bg-red-100 rounded-full dark:text-green-100 dark:bg-green-500">
                                            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path></svg>
                                        </div>
                                        <div>
                                        <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                                            Total not lime
                                        </p>
                                        <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                                            {{ $nonlime }}
                                        </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </main>
                    </div>
                </div>
            </div> --}}

            <div class="p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                <div class="flex items-center gap-3">
                    <div class="w-64 text-lg font-bold">Number of lime object</div>
                    <div class="w-64 text-lg font-bold">Number of non-lime object</div>
                    <div class="text-lg font-bold">Created date</div>
                </div>
                @forelse ($climes as $clime)
                <div class="flex items-center gap-3">
                    <div class="w-64">{{ $clime->lime }}</div>
                    <div class="w-64">{{ $clime->nonlime }}</div>
                    <div class="">{{ $clime->created_at }}</div>
                </div>
                @empty
                <div class="flex items-center gap-3">
                    <p class="w-32">No lime</p>
                    <p class="">N/A</p>
                </div>
                @endforelse
                {{ $climes->links() }}
            </div>
            {{-- <div class="p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                <div class="flex items-center gap-3">
                    <div class="w-32 text-lg font-bold">Search for apple</div>
                </div>
                @forelse ($apple as $apple)
                <div class="flex items-center gap-3">
                    <div class="w-32">{{ $apple->found }}</div>
                    <div class="">{{ $apple->qty }}</div>
                </div>
                @empty
                <div class="flex items-center gap-3">
                    <p class="w-32">No lime</p>
                    <p class="">N/A</p>
                </div>
                @endforelse
            </div> --}}

            <div class="grid gap-6 mb-8 md:grid-cols-2 xl:grid-cols-4 3xl:grid-cols-5 4xl:grid-cols-6 px-10">
                <!-- Card For Individual Content -->
                {{-- @foreach ($ideas as $idea)
                    <div class="bg-white p-4 rounded">
                        <div class="font-semibold text-xl ">{{ $idea->name }}</div>
                        <div class=""><svg class="w-16 h-16" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"></path></svg></div>
                        <div class="">Catagory : {{ $idea->catagory }}</div>
                        <div class="">{{ $idea->status }}</div>
                        <div class="">
                            <h3 class="">Description :</h3>
                            <p class="">{{ $idea->description }}</p>
                        </div>
                    </div>
                @endforeach --}}
            </div>

        </main>

    </body>
</html>
