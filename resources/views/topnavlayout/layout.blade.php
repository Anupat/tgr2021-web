<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TGR 19</title>

        <!-- Fonts -->
        {{-- <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet"> --}}
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Prompt:wght@300&display=swap" rel="stylesheet">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- Styles -->
        <style>
            body {
                font-family: 'Prompt', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <nav class="flex justify-between items-center px-6 py-3 shadow-md">
            <h1 class="font-bold text-3xl">
                <a href="" class="">TGR 19</a>
            </h1>
            <div class="flex">
                <div class="w-24 mr-6 text-yellow-500 rounded-full border-lg border-yellow-500 bg-white text-l font-semibold shadow">
                    <a href="{{ route('login') }}" class="flex justify-center p-3">Login</a>
                </div>
                <div class="w-24 text-yellow-500 rounded-full border-lg border-yellow-500 bg-white text-l font-semibold shadow">
                    <a href="{{ route('register') }}" class="flex justify-center p-3">Sign Up</a>
                </div>
            </div>
        </nav>
        @yield('content')
    </body>
</html>
