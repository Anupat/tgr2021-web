@extends('topnavlayout.layout')
@section('content')
        <main class="">
            <div class="mt-52">
                <div class="flex justify-center">
                    <div class="flex flex-col items-center">
                        <p class="text-5xl xl:text-6xl 3xl:text-8xl">มะนาวของ Bestmatch</p>
                        
                        <div class="flex gap-3 mt-6">
                            <div class="transition-all duration-500 ease-out w-max bg-gradient-to-r from-white to-white hover:from-green-400 hover:via-blue-500 hover:to-purple-400 hover:text-white rounded-full border-lg text-l font-semibold shadow">
                                <a href="dashboard" class="flex justify-center p-3 font-semibold text-xl mx-3">Enter Dashboard</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="">

            </div>
        </main>
@endsection